<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>e-Office | Sistem Disposisi Surat Disdikbud Tala</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('assets/adminbsb');?>/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('assets/adminbsb/');?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('assets/adminbsb/');?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url('assets/adminbsb/');?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url('assets/adminbsb/');?>css/style.css" rel="stylesheet">
    <style media="screen">
    @font-face { font-family: Bebas; src: url('<?=base_url();?>assets/font/BebasNeue.otf'); }
    @font-face { font-family: Mermaid; src: url('<?=base_url();?>assets/font/Mermaid1001.tff'); }

    @font-face {
    font-family: saira;
    src: url('<?=base_url();?>assets/font/Saira.ttf');
        }

    </style>
    <style type="text/css">
    /* Login Page ================================== */
    body{
        min-height: 100%;
        min-width: 100%;
        background-image: url(<?=base_url();?>assets/adminbsb/images/bg_main.jpg);
            background-size:100% !important;
            background-repeat: no-repeat !important;
            background-color: #00a1b3;
            padding: 0px;
            margin:0px;
            font-family: saira;
            position: fixed;


    }
        .card2{
                background: #fff;
                min-height: 100% !important;
                box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
                position: relative;
                margin-bottom: 30px;
                border-bottom-right-radius: 15px;
              border-bottom-left-radius: 15px;
              margin-bottom: 0px;
              padding-bottom: 0px;
              background-image: url(<?=base_url();?>assets/adminbsb/images/bg_form.png);
                background-size:cover;
                background-repeat: no-repeat !important;


        }
        .card2 .body2{

                font-size: 14px;
                color: #555;
                padding: 30px;

        }
        .form_login{
            margin-top: 0px;


        }
        .msg{
            text-align: center;
            padding:10px;
        }
        .logo_top{
            text-align: center;
            padding-bottom: 10px;
            padding-top: 20px;

        }

        .judul{
            padding-top: 100px;
            text-align: center;


        }
        .judul h3{
            font-family: saira;
            font-size: 16pt;

        }
        .title h4{
            font-family: saira;
        }
        h4{
            padding:0px;
            padding-top: -5px;

        }

        /* show it on small screens
        @media screen and (min-width: 0px) and (max-width: 600px) {
              body{
                  background-color: white;
                  padding: 0px;
                  margin:0px;
                  font-family: saira;

                }
                .judul{
                    padding-top: 10px;

                }
                .card2{
                    border-radius: 5px;
                }
                .logo_tala{
                    width: 100%;
                    padding-bottom: 20px;
                }
                h3{
                    display: none;
                    /*font-size: 12pt;

                }

            }

            @media screen and (min-width: 601px) and (max-width: 1024px) {
                 /* hide it elsewhere

            }
            */


            /*
    Max width before this PARTICULAR table gets nasty
    This query will take effect for any screen smaller than 760px
    and also iPads specifically.
    */
    @media
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
            body{
                  background: #00bcd4;
                  padding: 0px;
                  margin:0px;
                  font-family: saira;

                }

                .judul{
                    padding-top: 10px;

                }
                .card2{
                    border-radius: 5px;
                }
                .logo_tala{
                    width: 100%;
                    padding-bottom: 20px;
                }
                .logo_top img{
                    display: none;
                }
                h3{
                    display: none;


                }

    }

    /* Smartphones (portrait and landscape) ----------- */
    @media only screen
    and (min-device-width : 320px)
    and (max-device-width : 480px) {
        body {

        }

    /* iPads (portrait and landscape) ----------- */
    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        body {

        }
    }

    </style>

<!--
 <script type="text/javascript" src="<?php echo base_url();?>assets/redirection-mobile.js"></script><script type="text/javascript">// <![CDATA[
     SA.redirection_mobile ({
     mobile_url : "m.e-surat.diskominfo.tanahlautkab.go.id",
     });

    // ]]></script>
    -->
</head>

<body class="background">
<div class="container">

<?php
    $q_instansi = $this->db->query("SELECT * FROM tr_instansi LIMIT 1")->row();
    ?>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="judul">
                <img class="logo_tala animated bounceInRight" src="<?=base_url();?>/assets/adminbsb/images/logo_skpd.png" width="50%"  >
            <h3 class="animated bounceInLeft">
               Selamat Datang di Aplikasi <br>
               e-office Disdikbud Kabupaten Tanah Laut<br>

            </h3>

            </div>
        </div>


        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 form_login">
            <div class="card2 animated bounceInDown">
                <div class="body2 text-center">
                <div class="logo_top">
                    <img src="<?=base_url();?>assets/adminbsb/images/logo_tala.png" width="100px;">
                </div>
                <form id="login" method="POST" action="<?php echo base_URL(); ?>admin/do_login">
                    <?php
                    $status_login = $this->session->userdata('status_login');
                    if (empty($status_login)) {
                        $message = "Silahkan login untuk masuk ke aplikasi";
                    } else {
                        $message = $status_login;
                    }
                    ?>

                    <div class="title">

                        <h4>Login e-Office</h4>

                    </div>
                    <div class="msg"><?php echo $message; ?></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="u" placeholder="Username" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="p" placeholder="Password" required>
                        </div>
                    </div>
                  <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">perm_contact_calendar</i>
                        </span>
                        <div class="form-line">
                    <select name="ta" class="form-control" required><option value="">--</option>
                        <?php
                        for ($i = 2018; $i <= (date('Y')+1); $i++) {
                            if (date('Y') == $i) {
                                echo "<option value='$i' selected>$i</option>";
                            } else {
                                echo "<option value='$i'>$i</option>";
                            }
                        }
                        ?>
                        </select>
                                </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">LOGIN</button>
                        </div>
                    </div>

                   </form>
                </div>
            </div>
        </div>
    </div>

</div>



    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('assets/adminbsb/');?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('assets/adminbsb/');?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('assets/adminbsb/');?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url('assets/adminbsb/');?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('assets/adminbsb/');?>js/admin.js"></script>
    <script src="<?php echo base_url('assets/adminbsb/');?>js/pages/examples/sign-in.js"></script>
</body>

</html>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <head>
	<title> e-SURAT BPKAD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
	<style type="text/css">
	@font-face {
	  font-family: 'Cabin';
	  font-style: normal;
	  font-weight: 400;
	  src: local('Cabin Regular'), local('Cabin-Regular'), url(<?php echo base_url(); ?>assets/font/satu.woff) format('woff');
	}
	@font-face {
	  font-family: 'Cabin';
	  font-style: normal;
	  font-weight: 700;
	  src: local('Cabin Bold'), local('Cabin-Bold'), url(<?php echo base_url(); ?>assets/font/dua.woff) format('woff');
	}
	@font-face {
	  font-family: 'Lobster';
	  font-style: normal;
	  font-weight: 400;
	  src: local('Lobster'), url(<?php echo base_url(); ?>assets/font/tiga.woff) format('woff');
	}

	</style>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../bower_components/bootstrap/assets/js/html5shiv.js"></script>
      <script src="../bower_components/bootstrap/assets/js/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/jquery/jquery-ui.css" />

    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <!--<script src="<?php echo base_url(); ?>assets/js/bootswatch.js"></script>-->
	<script src="<?php echo base_url(); ?>assets/js/jquery/jquery-ui.js"></script>
	<!-- Mobile redirect script by https://pagecrafter.com -->
	<!--
    <script type="text/javascript" src="<?php echo base_url();?>assets/redirection-mobile.js"></script><script type="text/javascript">
    // <![CDATA[
     SA.redirection_mobile ({
        mobile_url : "m.e-surat.diskominfo.tanahlautkab.go.id",
        });

    //  ]]></script>
    -->
	<script type="text/javascript">
	// <![CDATA[
	$(document).ready(function () {
		$(function () {
			$( "#kode_surat" ).autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "<?php echo site_url('admin/get_klasifikasi'); ?>",
						data: { kode: $("#kode_surat").val()},
						dataType: "json",
						type: "POST",
						success: function(data){
							response(data);
						}
					});
				},
			});
		});

		$(function () {
			$( "#dari" ).autocomplete({
				source: function(request, response) {
					$.ajax({
						url: "<?php echo site_url('admin/get_instansi_lain'); ?>",
						data: { kode: $("#dari").val()},
						dataType: "json",
						type: "POST",
						success: function(data){
							response(data);
						}
					});
				},
			});
		});


		$(function() {
			$( "#tgl_surat" ).datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'yy-mm-dd'
			});
		});
	});
	// ]]>
	</script>
	</head>

  <body style="">
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
         <span class="navbar-brand"><strong style="font-family: verdana;">e-SURAT</strong></span>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
			         <li><a href="<?php echo base_url(); ?>admin"><i class="icon-home icon-white"> </i> Beranda</a></li>

		      <?php if($this->session->userdata('admin_level') != "Super Admin"){ ?>
            <li><a href="<?php echo base_url(); ?>admin/surat_masuk">Surat Masuk</a></li>
            <li><a href="<?php echo base_url(); ?>admin/surat_keluar">Surat Keluar</a></li>
            <?php  } ?>

            <!-- <?php
            if ($this->session->userdata('admin_level') == "Admin") {
            ?>
                <li><a href="<?php echo base_url(); ?>admin/surat_masuk">Surat Masuk</a></li>
                <li><a href="<?php echo base_url(); ?>admin/surat_keluar">Surat Keluar</a></li>

              <?php } ?> -->
			        <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"> Buku Agenda <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/agenda_surat_masuk"> Surat Masuk</a></li>
                <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/agenda_surat_keluar"> Surat Keluar</a></li>
              </ul>
            </li>

			<?php
			if ($this->session->userdata('admin_level') == "Super Admin") {
			?>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"> Referensi <span class="caret"></span></a>
          <ul class="dropdown-menu" aria-labelledby="themes">
          <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/klas_surat">Klasifikasi Surat</a></li>
          </ul>
      </li>
		<li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"><i class="icon-wrench icon-white"> </i> Pengaturan <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/pengguna">Instansi Pengguna</a></li>
                <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/manage_admin">Manajemen Admin</a></li>
              </ul>
            </li>

      <?php
			}
			?>


          </ul>

          <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('admin_nama');?> <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/passwod">Ganti Password</a></li>
                <li><a tabindex="-1" href="<?php echo base_url(); ?>admin/logout">Keluar</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>

	<?php
	$q_instansi	= $this->db->query("SELECT * FROM tr_instansi LIMIT 1")->row();
	echo $this->session->userdata('admin_level');
	?>
    <div class="container">

      <div class="page-header" id="banner">
        <div class="row">
          <div class="" style="padding: 15px 15px 0 15px;">
			<div class="well well-sm">
				<img src="<?php echo base_url(); ?>upload/<?php echo $q_instansi->logo; ?>" class="thumbnail span3" style="display: inline; float: left; margin-right: 20px; width: 100px; height: 100px">
                <h4 style="margin: 15px 0 10px 0; color: #000;"><b><?php echo $q_instansi->nama; ?></b></h4>
                <div style="color: #000; font-size: 16px; font-family: Tahoma" class="clearfix"> <?php echo $q_instansi->alamat; ?></div>
             </div>
          </div>
        </div>
      </div>

		<?php $this->load->view('admin/'.$page); ?>

	  <div class="span12 well well-sm">
      <center>
		<h5 style="font-weight: bold">e-OFFICE 1.0 &copy;2019 </br> <a href="" target="_blank">PEMKAB TANAH LAUT</a></a></h5>
      </center>
	  </div>

    </div>


</body></html>

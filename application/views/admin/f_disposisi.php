<?php
$q_instansi	= $this->db->query("SELECT * FROM tr_instansi LIMIT 1")->row();



// echo '<img src="'.base_url().'qr.png" />';
?>

<html>
<head>
<style type="text/css" media="print">
	table {border: solid 1px #000; border-collapse: collapse; width: 100%}
	tr { border: solid 1px #000}
	td { padding: 7px 5px}
	h3 { margin-bottom: -17px }
	h2 { margin-bottom: 0px }
</style>
<style type="text/css" media="screen">
	table {border: solid 1px #000; border-collapse: collapse; width: 60%}
	tr { border: solid 1px #000}
	td { padding: 7px 5px}
	h3 { margin-bottom: -17px }
	h2 { margin-bottom: 0px }
</style>
</head>

<body onload="window.print()">
<table>
	<tr><td colspan="3" >
		<center>
	<h3><?php echo $q_instansi->alamat; ?></h3>
	<h2><?php echo $q_instansi->nama; ?></h2>

</center>

	</td>



	</tr>

	<tr><td colspan="3" align="center" style="padding: 15px 0"><b style="font-size: 21px;">LEMBAR DISPOSISI</b></td></tr>
	<!-- <tr><td width="25%"><b>Indeks Berkas</b></td><td width="50%">: <?php echo $datpil1->indek_berkas; ?></td><td><b>Kode : </b><?php echo $datpil1->kode; ?></td></tr> -->
	<tr><td><b>Asal Surat</b></td><td colspan="2">: <?php echo $datpil1->dari; ?></td></tr>
	<tr><td width="25%"><b>Tanggal</b></td><td colspan="2">: <?php echo tgl_jam_sql($datpil1->tgl_surat)?></td></tr>
	<tr><td width="25%"><b>Nomor</b></td><td colspan="2">: <?php echo $datpil1->no_surat; ?></td></tr>
	<tr><td><b>Perihal</b></td><td colspan="2">: <?php echo $datpil1->isi_ringkas; ?></td></tr>
	<tr><td><b>Diterima Tanggal</b></td><td colspan="2">: <?php echo tgl_jam_sql($datpil1->tgl_diterima); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>No. Agenda &nbsp;&nbsp; </b> ( <?php echo $datpil1->no_agenda; ?> ) </td></tr>
	<tr><td colspan="3"><b>Tanggal Penyelesaian </b>: </td></tr>
	<tr><td style="height: 350px" valign="top" colspan="2"><b>Isi Disposisi : </b> <br><br>
		<?php


			if (!empty($datpil3)) {
				$klasifikasi 		=  array();
        foreach ($datpil3 as $d) {
					$klasifikasi['0'] 	= $d->isi_disposisi;
					$klasifikasi['1'] 	= $d->sifat;
					$klasifikasi['2'] 	= $d->batas_waktu;
				}
				$datanya= json_encode($klasifikasi);
				$qr['data'] 	=  "Isi Disposisi : ".$klasifikasi['0']." - Sifat : ".$klasifikasi['1']." - Batasan Waktu : ".$klasifikasi['2'];
				// $qr['data'] = $datanya;
				$qr['level'] 	= 'H';
				$qr['size'] 	= 3 ;
				$qr['savename'] = FCPATH.'qr.png';
				$this->ciqrcode->generate($qr);
				echo '<img  src="'.base_url().'qr.png" />';
			}
		?>

	<ol>
	<?php
	// if (!empty($datpil3)) {
	// 	foreach ($datpil3 as $d3) {
	// 		// echo '<img src="'.base_url('assets/images/').$d3->qrcode.'"></br>';
	// 		echo "<li><b><i>".$d3->isi_disposisi."</i></b>.</br>
	// 		Sifat: ".$d3->sifat."</br>
	// 		Batas Waktu : ".tgl_jam_sql($d3->batas_waktu)."</li>";
	// 	}
	// }
	?>
	</ol>


	</b></td><td valign="top" width="50%" style="border-left: solid 1px">
	Diteruskan kepada  :
	<ol>
	<?php
	if (!empty($datpil2)) {
		foreach ($datpil2 as $dp) {
			echo "<li>".$dp->kpd_yth."</li>";
		}
	}
	?>
	</ol>
	</td></tr>
	<!-- <tr><td colspan="3" style="line-height: 30px">Sesudah digunakan harap dikembalikan<br>
	Kepada : ........................................................................................................................................<br>
	Tanggal : ........................................................................................................................................<br>
	</td></tr> -->
</table>
</body>
</html>

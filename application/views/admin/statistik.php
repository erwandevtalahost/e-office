<div class="panel panel-info">
	<div class="panel-heading"><h3 style="margin-top: 5px">Selamat Datang <?php echo str_replace("_", " ", $this->session->userdata('admin_nama'));?></h3></div>
</div>
<?php if($this->session->userdata('admin_level') == "Super Admin" || $this->session->userdata('admin_level') == "Admin"){ ?>
<div class="panel panel-success">
	<div class="panel-heading">Data Surat Masuk dan Keluar Tahun <?php echo $this->session->userdata('admin_ta'); ?></div>
	<div class="panel-body">
		<div class="col-md-6">
			<b> Surat Masuk Berdasarkan Bulan</b>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Bulan</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$jml = 0;
					if (!empty($s_surat_masuk_bln)) {
						foreach ($s_surat_masuk_bln as $smb) {
							echo '<tr><td>'.$smb['bln'].'</td><td>'.$smb['jml'].'</td></tr>';
							$jml += $smb['jml'];
						}
					} else {
						echo '<tr><td colspan="2">tidak ada data</td></tr>';
					}
					?>
					<tr>
						<td>Jumlah Total</td>
						<td><?php echo $jml; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			<b> Surat Keluar Berdasarkan Bulan</b>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Bulan</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$jml2 = 0;
					if (!empty($s_surat_keluar_bln)) {
						foreach ($s_surat_keluar_bln as $skb) {
							echo '<tr><td>'.$skb['bln'].'</td><td>'.$skb['jml'].'</td></tr>';
							$jml2 += $skb['jml'];
						}
					} else {
						echo '<tr><td colspan="2">tidak ada data</td></tr>';
					}
					?>
					<tr>
						<td>Jumlah Total</td>
						<td><?php echo $jml2; ?></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="clearfix"></div>

		<!-- <div class="col-md-6">
			<b>Statistik Surat Masuk Berdasarkan Kode</b>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Kode</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if (!empty($s_surat_masuk_kode)) {
						foreach ($s_surat_masuk_kode as $smk) {
							echo '<tr><td>'.$smk['kode'].'</td><td>'.$smk['jml'].'</td></tr>';
						}
					} else {
						echo '<tr><td colspan="2">tidak ada data</td></tr>';
					}
					?>
				</tbody>
			</table>
		</div>

		<div class="col-md-6">
			<b>Statistik Surat Keluar Berdasarkan Kode</b>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Kode</th>
						<th>Jumlah</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if (!empty($s_surat_keluar_kode)) {
						foreach ($s_surat_keluar_kode as $skk) {
							echo '<tr><td>'.$skk['kode'].'</td><td>'.$smk['jml'].'</td></tr>';
						}
					} else {
						echo '<tr><td colspan="2">tidak ada data</td></tr>';
					}
					?>
				</tbody>
			</table>
		</div> -->
	</div>
</div>
<?php } ?>

<!-- Kepala Dinas -->
<?php if($this->session->userdata('admin_level') == "Kepala Dinas"){ ?>
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">Daftar Surat Masuk Belum Disposisi </h3>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				
				<?php 
					$q = "SELECT * FROM t_surat_masuk WHERE id NOT IN (SELECT id_surat FROM t_disposisi)";
					$dt = $this->db->query($q);
					if($dt->num_rows()>0){
				?>
						<div class="table-responsive">
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">No. Agenda</th>
			<th width="20%">Perihal, File</th>
			<th width="20%">Asal Surat</th>
			<th width="15%">Nomor, Tgl. Surat</th>
			<th width="15%">Ket. Disposisi</th>
			<th width="23%">Aksi</th>
		</tr>
	</thead>

	<tbody>
		<?php
		if (empty($dt->num_rows())) {
			echo "<tr><td colspan='5'  style='text-align: center; font-weight: bold'>--Data tidak ditemukan--</td></tr>";
		} else {
			$no 	= ($this->uri->segment(4) + 1);
			foreach ($dt->result() as $b) {
		?>
		<tr>
			<!-- <td><?php echo $b->no_agenda."/".$b->kode;?></td> -->
			<td><?php echo $b->no_agenda?></td>
			<td><?php echo $b->isi_ringkas."<br><b>File : </b><i><a href='".base_URL()."upload/surat_masuk/".$b->file."' target='_blank'>".$b->file."</a>"?></td>
			<td><?php echo $b->dari; ?></td>
			<td><?php echo $b->no_surat."<br><i>".tgl_jam_sql($b->tgl_surat)."</i>"?></td>
			<td>
				<?php
				$disposisi = $this->db->where('id_surat', $b->id)->get('t_disposisi');
				if($disposisi->num_rows()>0){
					$dis = $disposisi->row();
					echo "Kepada yth.<br><strong>".$dis->kpd_yth."</strong>";
				}else{
					echo "<p class='text-danger'><b>Belum disposisi</b></p>";
				}
				?>
			</td>

			<td class="ctr">
			
				
				<div class="btn-group">
				    
					<a href="<?php echo base_URL()?>admin/surat_disposisi/<?php echo $b->id?>" class="btn btn-danger btn-sm"  title="Disposisi Surat"><i class="icon-trash icon-list"> </i> Isi Disposisi</a>
			</div>
				

			</td>
		</tr>
		<?php
			$no++;
			}
		}
		?>
	</tbody>
</table>
	</div>
				<?php } else{
						echo "<div class='alert alert-info'>Tidak ada data !</div>";
					}?>
		</div>
	</div>


 <?php } ?>
		

<!-- Sekretaris Level -->
<!--<?php if($this->session->userdata('admin_level') == "Sekretaris"){ ?>-->
<!--	<div class="panel panel-success">-->
<!--		<div class="panel-heading">-->
<!--			<h3 class="panel-title">Daftar Disposisi </h3>-->
<!--		</div>-->
<!--		<div class="panel-body">-->
<!--			<div class="table-responsive">-->
				
<!--		</div>-->
<!--	</div>-->


<!-- <?php } ?>-->

<!-- Kabid Level -->

<?php if($this->session->userdata('admin_level') != "Super Admin" && $this->session->userdata('admin_level') != "Admin" && $this->session->userdata('admin_level') != "Kepala Dinas"){ ?>

<div class="row">

</div>
<?php
	$dt = $this->db->where('t_disposisi.kpd_yth', $this->session->userdata('admin_level'))->join('t_surat_masuk', 't_surat_masuk.id = t_disposisi.id_surat')->order_by('t_disposisi.id','DESC')->get('t_disposisi');

	if($dt->num_rows()>0){ ?>
		<div class="panel panel-success">
		  <div class="panel-heading">
		    <h3 class="panel-title">Daftar Disposisi </h3>
		  </div>
		  <div class="panel-body">
				<div class="table-responsive">
				<table width="100%" border="1" cellspacing="0" cellpadding="0" class="table table-bordered table-hover">
				  <thead>
				    <tr style="background-color: black; color: white;">
				      <td width="4%" align="center"><strong>No. Agenda</strong></td>
				      <td width="19%" align="center"><strong>Asal Surat, Perihal, File</strong></td>
				      <td width="13%" align="center"><strong>Nomor, Tgl. Surat</strong></td>
				      <td width="17%" align="center"><strong>Disposisi</strong></td>
				      <td width="27%" align="center"><strong>Isi</strong></td>
				      <td align="center"><strong>Sifat, Batasan, Catatan</strong></td>
				      <td align="center"><strong>Aksi</strong></td>
			        </tr>
					</thead>
					<tbody>
					  <?php
							$no 	= ($this->uri->segment(4) + 1);
							foreach ($dt->result() as $b) {
						?>
				    <tr>
				      <td rowspan="3" valign="top"><?php echo $b->no_agenda?></td>
				      <td valign="top">
						Asal Surat						: <b> <?php echo $b->dari; ?></b>
					</td>
				      <td rowspan="3" valign="top"><?php echo $b->no_surat."<br><i>".tgl_jam_sql($b->tgl_surat)."</i>"?></td>
				      <td rowspan="3" valign="top">Yth.<br><b><?php echo $b->kpd_yth; ?></td>
				      <td rowspan="3" valign="top"><?php echo $b->isi_disposisi; ?></td>
				      <td width="15%" valign="top">Sifat : <?php echo $b->sifat;?></td>
				      <td width="5%" rowspan="3" align="center" valign="middle"><a href="<?=base_url('admin/disposisi_cetak/'.$b->id);?>" class="btn btn-info btn-sm" target="_blank"><i class="icon-print icon-white"> </i></i></a></td>
			        </tr>
				    <tr>
				      <td valign="top">Perihal :
			          <b><?php echo $b->isi_ringkas; ?></b></td>
				      <td valign="top">Batasan : <?php echo $b->batas_waktu;?></td>
				      </tr>
				    <tr>
				      <td valign="top">File : <a href="<?=base_URL('upload/surat_masuk/');?><?=$b->file;?>" target="_blank">
						  <?=$b->file;?></a></td>
				      <td valign="top">Catatan : <strong><?php echo $b->catatan;?></strong></td>
				      </tr>
					  <?php
							$no++;
							}

						?>
			      </tbody>
				  </table>
				</div>
          </div>
		</div>


	<?php }else{
		echo "<div class='alert alert-info'>tidak ada disposisi</div>";
	}
?>
 <?php } ?>
</div>

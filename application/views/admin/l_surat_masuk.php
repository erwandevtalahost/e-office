<div class="clearfix">

<div class="panel panel-info">
	<div class="panel-heading" style="overflow: auto">
		<div class="col-md-2"><h3 style="margin-top: 5px">Surat Masuk</h3></div>
		<div class="col-md-2">
			<?php
			if ($this->session->userdata('admin_level')=="Admin") {
			?>
			<a href="<?php echo base_URL(); ?>admin/surat_masuk/add" class="btn btn-info"><i class="icon-plus-sign icon-white"> </i> Tambah Data</a>
		<?php } ?>
		</div>
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<form class="navbar-form navbar-left" method="post" action="<?php echo base_URL(); ?>admin/surat_masuk/cari" style="margin-top: 0px">
				<input type="text" class="form-control" name="q" style="width: 200px" placeholder="Kata kunci pencarian ..." required>
				<button type="submit" class="btn btn-danger"><i class="icon-search icon-white"> </i> Cari</button>
			</form>
		</div>
	</div>
</div>

<?php echo $this->session->flashdata("k");?>
<div class="table-responsive">
<table class="table table-bordered table-hover" width="100%">
	<thead>
		<tr>
			<th width="5%">No. Agenda</th>
			<th >Perihal, File</th>
			<th>Asal Surat</th>
			<th >Nomor, Tgl. Surat</th>
			<th >Ket. Disposisi</th>
			<th >Aksi</th>
		</tr>
	</thead>

	<tbody>
		<?php
		if (empty($data)) {
			echo "<tr><td colspan='5'  style='text-align: center; font-weight: bold'>--Data tidak ditemukan--</td></tr>";
		} else {
			$no 	= ($this->uri->segment(4) + 1);
			foreach ($data as $b) {
			    if(!empty($b->file)){
			        $file = "<a href='".base_URL()."upload/surat_masuk/".$b->file."' target='_blank'>Lihat</a>";
			    }else{
			        $file="Tidak tersedia";
			    }
		?>
		<tr>
			<!-- <td><?php echo $b->no_agenda."/".$b->kode;?></td> -->
			<td><?php echo $b->no_agenda?></td>
			<td><?php echo $b->isi_ringkas."<br><b>File : </b>".$file?></td>
			<td><?php echo $b->dari; ?></td>
			<td><?php echo $b->no_surat."<br><i>".tgl_jam_sql($b->tgl_surat)."</i>"?></td>
			<td>
				<?php
				$disposisi = $this->db->where('id_surat', $b->id)->get('t_disposisi');
				if($disposisi->num_rows()>0){
					$dis = $disposisi->row();
					echo "Kepada yth.<br><strong>".$dis->kpd_yth."</strong>";
				}else{
					echo "<p class='text-danger'><b>Belum disposisi</b></p>";
				}
				?>
			</td>

			<td class="ctr">
				<?php
				if ($this->session->userdata('admin_level')=="Admin" || $this->session->userdata('admin_level')=="Kepala Dinas") {
				?>

				<div class="btn-group">
					<a href="<?php echo base_URL()?>admin/surat_disposisi/<?php echo $b->id?>" class="btn btn-default btn-sm"  title="Disposisi Surat"><i class="icon-trash icon-list"> </i> Disposisi</a>
					<a href="<?php echo base_URL()?>admin/disposisi_cetak/<?php echo $b->id?>" class="btn btn-info btn-sm" target="_blank" title="Cetak Disposisi"><i class="icon-print icon-white"> </i> </a>
					<a href="<?php echo base_URL()?>admin/surat_masuk/edt/<?php echo $b->id?>" class="btn btn-success btn-sm" title="Edit Data"><i class="icon-edit icon-white"> </i> </a>
					<a href="<?php echo base_URL()?>admin/surat_masuk/del/<?php echo $b->id?>" class="btn btn-warning btn-sm" title="Hapus Data" onclick="return confirm('Anda Yakin..?')"><i class="icon-trash icon-remove">  </i> </a>
				</div>
				<?php
				} else {
				?>
				<div class="btn-group">
					<a href="<?php echo base_URL()?>admin/disposisi_cetak/<?php echo $b->id?>" class="btn btn-info btn-sm" target="_blank" title="Cetak Disposisi"><i class="icon-print icon-white"> </i> Ctk</a>
				</div>
				<?php
				}
				?>

			</td>
		</tr>
		<?php
			$no++;
			}
		}
		?>
	</tbody>
</table>
	</div>
<center><ul class="pagination"><?php echo $pagi; ?></ul></center>
</div>

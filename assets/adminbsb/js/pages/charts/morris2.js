$(function () {
    // getMorris('line', 'line_chart');
    getMorris('bar', 'bar_chart');
    getMorris('area', 'area_chart');
    getMorris('donut', 'donut_chart');
});


function getMorris(type, element) {
    // if (type === 'line') {
    //     Morris.Line({
    //         element: element,
    //         data: [{
    //             'period': '2011 Q3',
    //             'licensed': 3407,
    //             'sorned': 660
    //         }, {
    //                 'period': '2011 Q2',
    //                 'licensed': 3351,
    //                 'sorned': 629
    //             }, {
    //                 'period': '2011 Q1',
    //                 'licensed': 3269,
    //                 'sorned': 618
    //             }, {
    //                 'period': '2010 Q4',
    //                 'licensed': 3246,
    //                 'sorned': 661
    //             }, {
    //                 'period': '2009 Q4',
    //                 'licensed': 3171,
    //                 'sorned': 676
    //             }, {
    //                 'period': '2008 Q4',
    //                 'licensed': 3155,
    //                 'sorned': 681
    //             }, {
    //                 'period': '2007 Q4',
    //                 'licensed': 3226,
    //                 'sorned': 620
    //             }, {
    //                 'period': '2006 Q4',
    //                 'licensed': 3245,
    //                 'sorned': null
    //             }, {
    //                 'period': '2005 Q4',
    //                 'licensed': 3289,
    //                 'sorned': null
    //             }],
    //         xkey: 'period',
    //         ykeys: ['licensed', 'sorned'],
    //         labels: ['Licensed', 'Off the road'],
    //         lineColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)'],
    //         lineWidth: 3
    //     });
    // } else 
    if (type === 'bar') {
        Morris.Bar({
            element: element,
            data: [
            {
                x: 'SKPD Q1',
                y: 3,
                z: 2,
                a: 3
            }, {
                    x: 'SKPD Q2',
                    y: 2,
                    z: null,
                    a: 1
                }, {
                    x: 'SKDP Q3',
                    y: 0,
                    z: 2,
                    a: 4
                }, {
                    x: 'SKPD Q4',
                    y: 2,
                    z: 4,
                    a: 3
                }],
            xkey: 'x',
            ykeys: ['y', 'z', 'a'],
            labels: ['Y', 'Z', 'A'],
            barColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(0, 150, 136)'],
        });
    } 
}